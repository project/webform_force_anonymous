Webform force anonymous
=======================

Webform Force Anonymous is a very simple, lightweight module that gives the
creator of a webform the option to force anonymous submissions for logged-in
users who submitted the webform.

REQUIREMENTS
------------

This module requires the following module:
 * Webform (https://www.drupal.org/project/webform).

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
     https://www.drupal.org/docs/7/extend/installing-modules
     for further information.

CONFIGURATION
-------------

 * Setup your webforms by editing a webform node and going to the Webform tab,
     and then clicking Form settings, scrolling down to Anonymize IP Settings.

CREDITS
-------

Inspired from Webform Secret IP module (https://www.drupal.org/project/webform_secret_ip).
